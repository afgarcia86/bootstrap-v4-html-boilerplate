var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');

gulp.task('sass', function () {
  gulp.src('./sass/**.scss')
    .pipe(sass({includePaths: ['./sass']}).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(gulp.dest('./'))
});

gulp.task('default', ['sass'], function() {
	// gulp watch for sass changes
  gulp.watch('./sass/**/*', ['sass']);
});