# Bootstrap v4 HTML Boilerplate

1. [install Node](https://docs.npmjs.com/getting-started/installing-node)
1. `$ npm install`
1. `$ gulp`
1. Have fun!

### SASS Tips

- Break up sass files based on the template the styles are for
- Add custom template sass files to the `sass/themes/` folder name them `_...`
- Use variables ex: `$black = #000` instead of hard coding colors
- Include the files in the `style.scss`
- Watch the `gulp` console to see it auto compile and check for errors.

### JavaScript Tips

- Put any JS plugins you download in the `js/vendors` folder.
- Custom JS can just go in the root `JS` folder
- Break up JS fules based on the page template they are for
- Update footer of each page to include the required scripts only

### Resources

- [Bootstrap v4 docs](http://v4-alpha.getbootstrap.com/getting-started/introduction/)
- [Using SASS](http://sass-lang.com/guide)